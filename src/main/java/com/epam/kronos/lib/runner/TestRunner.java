package com.epam.kronos.lib.runner;

import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Valiantsin_Serada on 9/7/2016.
 */
public class TestRunner {
    public static void main(String[] args) {
        final String NAME_XML_SUITE = "TmpSuite";
        final String SUITE = "./src/main/resources/Suite.xml";
        TestNG tng = new TestNG();
        XmlSuite suite = new XmlSuite();
        suite.setName(NAME_XML_SUITE);
        List<String> files = new ArrayList<String>();
        files.addAll(new ArrayList<String>() {
            {
                add(SUITE);
            }
        });
        suite.setSuiteFiles(files);
        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(suite);
        tng.setXmlSuites(suites);
        tng.run();
    }
}
