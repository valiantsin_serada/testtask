package com.epam.kronos.lib.screen;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by Valiantsin_Serada on 9/7/2016.
 */
public class GooglePage {
    public static final String INPUT_FIELD = "//*[@id='lst-ib']";
    public static final String SEARCH_LINK_PATTERN = "//a[@href='%s']";
    public static final String BASE_URL = "https://www.google.com";
    public WebDriver driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),  DesiredCapabilities.chrome());


   public GooglePage() throws MalformedURLException {
       driver.manage().window().maximize();
       driver.manage()
               .timeouts()
               .pageLoadTimeout(20,
                       TimeUnit.SECONDS);
       driver.manage()
               .timeouts()
               .implicitlyWait(10,
                       TimeUnit.SECONDS);
   }

    public void open()throws MalformedURLException {
        driver.navigate().to(BASE_URL);
    }

    public void search(String searchQuery) throws MalformedURLException, InterruptedException {
        driver.findElement(By.xpath(INPUT_FIELD)).sendKeys(searchQuery);
        driver.findElement(By.xpath(INPUT_FIELD)).submit();
    }

    public boolean isLinkPresent(String link){
        try {
            driver.findElement(By.xpath(String.format(SEARCH_LINK_PATTERN, link)));
            return true;
        }catch(Exception e){
            return false;
        }
    }
}
