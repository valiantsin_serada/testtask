package com.epam.kronos.lib.services;

import com.epam.kronos.lib.screen.GooglePage;
import java.net.MalformedURLException;

/**
 * Created by Valiantsin_Serada on 9/7/2016.
 */
public class SearchService {
    GooglePage googlePage = new GooglePage();

    public SearchService() throws MalformedURLException {
    }

    public void openGooglePage() throws MalformedURLException {
        googlePage.open();
    }

    public void searchInGoogle(String searchQuery) throws MalformedURLException, InterruptedException {
        googlePage.search(searchQuery);
    }

    public boolean isLinkPresent(String link){
        return googlePage.isLinkPresent(link);
    }

    public void quit(){
        googlePage.driver.quit();
    }
}
