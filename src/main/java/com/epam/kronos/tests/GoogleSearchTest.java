package com.epam.kronos.tests;

import com.epam.kronos.lib.services.SearchService;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.MalformedURLException;

/**
 * Created by Valiantsin_Serada on 9/7/2016.
 */
public class GoogleSearchTest {
    SearchService searchService = new SearchService();
    public static final String query = "EPAM";
    public static final String searchedLink = "https://www.epam.com/";

    public GoogleSearchTest() throws MalformedURLException {
    }

    @BeforeClass
    public void openPage() throws MalformedURLException {
        searchService.openGooglePage();
    }

    @Test
    public void search() throws MalformedURLException, InterruptedException {
        searchService.searchInGoogle(query);
        Assert.assertTrue(searchService.isLinkPresent(searchedLink));
    }

    @AfterClass
    public void closeBrowser(){
        searchService.quit();
    }
}
